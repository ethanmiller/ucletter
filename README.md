# README #

### What is this repository for? ###

* This repository contains a template for University of California department letterhead in LaTeX.  It's intended for use by University of California employees and students.
* Current version: 1.1

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Please send updates to Professor Ethan L. Miller (elm <at> cs.ucsc.edu).