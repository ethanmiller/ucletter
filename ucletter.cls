% UC letterhead class
%
% Use the letterhead in the following way:
%
% \documentclass[ucsc,cs,shortfrom,11pt]{ucletter}
%
% \telephone{+1 (831) 459-1222}
% \email{elm@cs.ucsc.edu}
%
% This will set the document up for Computer Science at UCSC.
%
% There are many campuses and departments supported; if you'd like additional ones added,
% please provide the information in the same format as that for an existing department.
% Note that you might need to provide campus-wide information as well as per-department
% info if your campus isn't already listed.
%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ucletter}

\RequirePackage{ifthen}
\RequirePackage{graphicx}
\RequirePackage{mathptmx}
\RequirePackage{times}
\RequirePackage[absolute]{textpos}

% Default is short return address
\newcommand{\ucl@fromaddr}{short}

% Defaults are for the Computer Science Department at UCSC...
\newcommand{\ucl@univ}{UCSC}
\newcommand{\ucl@campus}{Santa Cruz}
\newcommand{\ucl@division}{Baskin School of Engineering}
\newcommand{\ucl@univstreet}{Engineering 2 Building}
\newcommand{\ucl@univtown}{Santa Cruz, California 95064}
\newcommand{\ucl@dept}{Computer Science Department}
\newcommand{\ucl@deptphone}{+1 (831) 459-2747}
\newcommand{\ucl@deptfax}{+1 (831) 459-4829}
\newcommand{\ucl@depturl}{http://www.cs.ucsc.edu/}
\newcommand{\ucl@telephone}{}
\newcommand{\ucl@cellphone}{}
\newcommand{\ucl@email}{}
\newcommand{\ucl@fax}{}
\newcommand{\dept}[1]{\renewcommand{\ucl@dept}{#1}}
\newcommand{\uccampus}[1]{\renewcommand{\ucl@campus}{#1}}
\newcommand{\ucdivision}[1]{\renewcommand{\ucl@division}{#1}}
\newboolean{ucl@firstfooter}
\setboolean{ucl@firstfooter}{false}
\newboolean{ucl@otherfooter}
\setboolean{ucl@otherfooter}{false}
\newcommand{\ucl@pagetitle}{}

% These commands show up in the return address, but NOT in the footer on each page
\newcommand{\email}[1]{\renewcommand{\ucl@email}{#1}}
\newcommand{\fax}[1]{\renewcommand{\ucl@fax}{#1}}


\DeclareOption{ucsc}{
  \renewcommand{\ucl@univ}{UCSC}
  \renewcommand{\ucl@campus}{Santa Cruz}
  \renewcommand{\ucl@depturl}{http://www.soe.ucsc.edu/}
  \renewcommand{\ucl@deptphone}{+1 (831) 459-2158}
  \renewcommand{\ucl@deptfax}{+1 (831) 459-4829}
  \renewcommand{\ucl@univtown}{Santa Cruz, California 95064}
  \renewcommand{\ucl@division}{Jack Baskin School of Engineering}
  \DeclareOption{soe}{
    \dept{Jack Baskin Division of Engineering}
    \renewcommand{\ucl@deptphone}{+1 (831) 459-2158}
    \renewcommand{\ucl@deptfax}{+1 (831) 459-4829}
    \renewcommand{\ucl@depturl}{http://www.soe.ucsc.edu/}
    \renewcommand{\ucl@univstreet}{Baskin Engineering Building}
  }
  \DeclareOption{ams}{
    \dept{Applied Mathematics \& Statistics}
    \renewcommand{\ucl@deptphone}{+1 (831) 459-2747}
    \renewcommand{\ucl@deptfax}{+1 (831) 459-4829}
    \renewcommand{\ucl@depturl}{http://www.ams.ucsc.edu/}
    \renewcommand{\ucl@univstreet}{Baskin Engineering Building}
  }
  \DeclareOption{bme}{
    \dept{Biomolecular Engineering Department}
    \renewcommand{\ucl@deptphone}{+1 (831) 459-2747}
    \renewcommand{\ucl@deptfax}{+1 (831) 459-4829}
    \renewcommand{\ucl@depturl}{http://www.bme.ucsc.edu/}
    \renewcommand{\ucl@univstreet}{Baskin Engineering Building}
  }
  \DeclareOption{ce}{
    \dept{Computer Engineering Department}
    \renewcommand{\ucl@univstreet}{Engineering 2 Building}
    \renewcommand{\ucl@deptphone}{+1 (831) 459-5752}
    \renewcommand{\ucl@deptfax}{+1 (831) 459-4829}
    \renewcommand{\ucl@depturl}{http://www.ce.ucsc.edu/}
  }
  \DeclareOption{cs}{
    \dept{Computer Science Department}
    \renewcommand{\ucl@deptphone}{+1 (831) 459-2747}
    \renewcommand{\ucl@deptfax}{+1 (831) 459-4829}
    \renewcommand{\ucl@depturl}{http://www.cs.ucsc.edu/}
    \renewcommand{\ucl@univstreet}{Engineering 2 Building}
  }
  \DeclareOption{ee}{
    \dept{Electrical Engineering Department}
    \renewcommand{\ucl@deptphone}{+1 (831) 459-5752}
    \renewcommand{\ucl@deptfax}{+1 (831) 459-4829}
    \renewcommand{\ucl@depturl}{http://www.ee.ucsc.edu/}
    \renewcommand{\ucl@univstreet}{Baskin Engineering Building}
  }
}

\DeclareOption{ucsb}{
  \renewcommand{\ucl@univ}{UCSB}
  \renewcommand{\ucl@campus}{Santa Barbara}
  \renewcommand{\ucl@division}{}
  \DeclareOption{cs}{
    \renewcommand{\ucl@dept}{Computer Science Department}
    \renewcommand{\ucl@deptphone}{+1 (805) 893-4321}
    \renewcommand{\ucl@depturl}{http://www.cs.ucsb.edu/}
    \renewcommand{\ucl@univstreet}{}
    \renewcommand{\ucl@univtown}{Santa Barbara, California 93106}
  }
}

\DeclareOption{ucd}{
  \renewcommand{\ucl@univ}{UCD}
  \renewcommand{\ucl@campus}{Davis}
  \renewcommand{\ucl@division}{College of Engineering}
  \renewcommand{\ucl@univstreet}{2064 Kemper Hall, One Shields Avenue}
  \renewcommand{\ucl@univtown}{Davis, California 95616}
  \DeclareOption{ece}{
    \dept{Department of Electrical and Computer Engineering}
    \renewcommand{\ucl@deptphone}{+1 (530) 752-0583}
    \renewcommand{\ucl@division}{College of Engineering}
    \renewcommand{\ucl@deptfax}{+1 (530) 752-8428}
    \renewcommand{\ucl@depturl}{http://www.ece.ucdavis.edu/}
  }
}

\DeclareOption{nofrom}{
  \renewcommand{\ucl@fromaddr}{none}
}

\DeclareOption{shortfrom}{
  \renewcommand{\ucl@fromaddr}{short}
}

\DeclareOption{fullfrom}{
  \renewcommand{\ucl@fromaddr}{full}
}

\DeclareOption{datefrom}{
  \renewcommand{\ucl@fromaddr}{date}
}

\DeclareOption{noaddrfooter}{
  \setboolean{ucl@firstfooter}{false}
  \setboolean{ucl@otherfooter}{false}
}

\DeclareOption{addrfooterall}{
  \setboolean{ucl@firstfooter}{true}
  \setboolean{ucl@otherfooter}{true}
}

\DeclareOption{addrfooter}{
  \setboolean{ucl@firstfooter}{false}
  \setboolean{ucl@otherfooter}{true}
}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{letter}}
\ProcessOptions

\LoadClass{letter}
% This has to be done after loading the letter class to override geometry
\RequirePackage[letterpaper,text={7in,9in},includeheadfoot,headsep=0.4in,centering]{geometry}

\renewcommand{\telephone}[1]{\renewcommand{\ucl@telephone}{#1}}
\newcommand{\cellphone}[1]{\renewcommand{\ucl@cellphone}{#1}}
\newcommand{\@sixrm}{\fontfamily{ptm}\fontsize{6}{8}\selectfont}
\newcommand{\@svnrm}{\fontfamily{ptm}\fontsize{7}{9}\selectfont}
\newcommand{\@egtrm}{\fontfamily{ptm}\fontsize{8}{10}\selectfont}
\newcommand{\@twlrm}{\fontfamily{ptm}\fontsize{12}{14}\selectfont}
\newcommand{\@ctrdot}{\hspace*{1.4pt}$\cdot$\hspace*{1.4pt}}

% Build the UC letterhead
\newcommand{\letterhead}{
  \begin{textblock*}{7in}(0.75in,0.5in)
    \begin{tabular*}{7in}{@{}l@{\extracolsep\fill}r@{}}
      \@twlrm\selectfont UNIVERSITY OF CALIFORNIA, {\MakeUppercase \ucl@campus} &
    \end{tabular*}\\
    \begin{tabular*}{7in}{@{}l@{}c@{}r@{}}
      \parbox{4.60in}{\rule{4.55in}{0.25pt} \\
        \@svnrm BERKELEY\@ctrdot DAVIS\@ctrdot IRVINE\@ctrdot
        LOS ANGELES\@ctrdot MERCED\@ctrdot RIVERSIDE\@ctrdot
        SAN DIEGO\@ctrdot SAN FRANCISCO\hspace\fill \\
        \rule[3pt]{4.55in}{0.25pt}} &
      \parbox{0.75in}{\resizebox{0.75in}{!}{\includegraphics{ucsealnew}}}
      \parbox{1.6in}{\rule{1.6in}{0.25pt} \\
        \rule{0mm}{4pt}\hspace\fill
        \@svnrm SANTA BARBARA\@ctrdot SANTA CRUZ\hspace*{1pt} \\
        \rule[3pt]{1.6in}{0.25pt}}
    \end{tabular*} \\
    \vspace*{24pt}
    \@egtrm
    \begin{tabular*}{7in}{@{}l@{\extracolsep\fill}r@{}}
      \MakeUppercase \ucl@dept & \MakeUppercase \ucl@deptphone \\
      \ifthenelse{\equal{\ucl@division}{}}{\MakeUppercase \ucl@univstreet}{\MakeUppercase \ucl@division} & \MakeUppercase FAX \ucl@deptfax \\
      \ifthenelse{\equal{\ucl@division}{}}{\MakeUppercase \ucl@univtown}{\MakeUppercase \ucl@univstreet} & \\
      \ifthenelse{\equal{\ucl@division}{}}{}{\MakeUppercase \ucl@univtown} & \\
    \end{tabular*}
  \end{textblock*}
}

\newcommand{\@ucaddress}{
  \begin{minipage}[t]{2.2in}
    \ifthenelse{\equal{\ucl@fromaddr}{full}}{
      \ifthenelse{\equal{\ucl@dept}{}}{}{\ucl@dept\\}
      \ifthenelse{\equal{\ucl@division}{}}{}{\ucl@division\\}
      University of California \\
      \ifthenelse{\equal{\ucl@univstreet}{}}{}{\ucl@univstreet\\}
      \ucl@univtown \\
    }
    {}
    \ifthenelse{\equal{\ucl@fromaddr}{date}}{}{
      \ifthenelse{\equal{\ucl@telephone}{}}{}{{\sc Phone:} \ucl@telephone\\}
      \ifthenelse{\equal{\ucl@cellphone}{}}{}{{\sc Cell:} \ucl@cellphone\\}
      \ifthenelse{\equal{\ucl@fax}{}}{}{{\sc Fax:} \ucl@fax\\}
      \ifthenelse{\equal{\ucl@email}{}}{}{{\sc Email:} \ucl@email \\}
    }
    \@date
  \end{minipage}
}

% The argument is for a page number, in case you want to print the page number at
% the bottom of the page
\newcommand{\@addrfoot}[1]{
  % return address in 9 point font
  \fontfamily{ptm}\fontsize{8}{10}\selectfont \sc
  \begin{tabular}{c@{\hspace{7pt}$\cdot$\hspace{7pt}}c@{\hspace{7pt}$\cdot$\hspace{7pt}}c}
    \ucl@dept & \ucl@univstreet & \ucl@univtown \\
    Phone: \ucl@deptphone & Fax: \ucl@deptfax & \ucl@depturl
  \end{tabular}
}

\newcommand{\pagetitle}[1]{\renewcommand{\ucl@pagetitle}{#1}}

\renewcommand{\ps@headings}{
  % Right justify the page number
  \renewcommand{\@oddhead}{\hspace*{\fill}\ucl@pagetitle\hspace*{\fill}\thepage}
  \renewcommand{\@evenhead}{\@oddhead}
  % Pass the page number to the footer, which doesn't do anything currently
  \renewcommand{\@oddfoot}{\ifthenelse{\boolean{ucl@otherfooter}}{\@addrfoot{\thepage}}{}}
  \renewcommand{\@evenfoot}{\@oddfoot}
}

\pagestyle{headings}

\renewcommand{\opening}[1]{
  \letterhead
  \thispagestyle{empty}
  \vspace*{0.60in}
  \ifthenelse{\equal{\ucl@fromaddr}{none}}{}{
    \noindent\rule{4.5in}{0mm}
    \@ucaddress
  }
  \par\noindent\toname \\ \toaddress

  \vspace{1ex}

  \noindent #1
}
